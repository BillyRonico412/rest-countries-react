# rest-countries-react
Le projet de rest countries API de front end mentor codé en React
https://www.frontendmentor.io/challenges/rest-countries-api-with-color-theme-switcher-5cacc469fec04111f7b848ca/hub/rest-countries-api-with-color-theme-switcher-7SFr5lkuI

## Utilisation de React
- TypeScript
- Hooks
- React Router
- Docker
- Testing (A voir)
